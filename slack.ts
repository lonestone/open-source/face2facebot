import axios from 'axios'
import * as qs from 'qs'
import settings from './settings'

interface User {
  id: string
  name: string
}

interface Channel {
  id: string
  name: string
  members: string[] // users ids
}

// https://api.slack.com/methods/channels.list
export async function getChannels(): Promise<Channel[]> {
  const result = await axios.get(
    `https://slack.com/api/channels.list?token=${settings.slackToken} `
  )
  return result.data.channels
}

// https://api.slack.com/methods/users.list/test
export async function getUsers(): Promise<User[]> {
  const result = await axios.get(
    `https://slack.com/api/users.list?token=${settings.slackToken}`
  )
  return result.data.members
}

// Send message with Slackbot
export function sendMessage(message: string) {
  const params = qs.stringify({
    token: settings.slackbotToken,
    channel: `#${settings.channel}`
  })
  axios.post(`${settings.slackbotUrl}?${params}`, message)
}
