export default {
  slackToken: 'XXX',
  slackbotUrl: 'https://lonestone.slack.com/services/hooks/slackbot',
  slackbotToken: 'XXX',

  // Channel to post
  channel: 'daily-stuff',

  message: `*Face 2 Face* :busts_in_silhouette::speech_balloon:
Cette semaine, les meetings 1/1 sont à faire entre :

{list}

Prenez 15 min pour parler de ce que vous ressentez (en bien et mal), au niveau pro et perso.
Essayez de trouver les points de blocage ou de frustration.
Proposez les idées qui en découlent sur ce channel ou à un mousquetaire.
À vous de jouer :wink:`
}
