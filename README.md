Face2Face Slackbot
==================

Every week, pick random pairs of Slack users to setup 1/1 meetings.

# Install

`yarn`

# Start / send message

`yarn start`

# Build

`yarn build`

# Cron

`0 10 * * 2 /usr/local/bin/node /home/lonestone/face2face/dist/index.js`
