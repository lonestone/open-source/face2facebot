import settings from './settings'
import { sendMessage, getChannels, getUsers } from './slack'

interface IUserPair {
  user1: string
  user2: string
}

const pairs: IUserPair[] = []

const pickRandom = <T>(list: T[]): T => {
  const index = Math.floor(list.length * Math.random())
  const [item] = list.splice(index, 1)
  return item
}

async function run() {
  const channels = await getChannels()
  const users = await getUsers()

  const channel = channels.find(c => c.name === settings.channel)
  if (!channel) {
    throw new Error(`Channel ${settings.channel} not found
Channel names are: ${channels.map(c => c.name).join(', ')}`)
  }
  const usernames = channel.members.map(id => users.find(u => u.id === id)!.name)

  while (usernames.length >= 2) {
    pairs.push({
      user1: pickRandom(usernames),
      user2: pickRandom(usernames)
    })
  }

  sendMessage(
    settings.message.replace(
      '{list}',
      pairs.map(pair => `- @${pair.user1} et @${pair.user2}`).join('\n')
    )
  )
}

run()
